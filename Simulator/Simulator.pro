CONFIG += c++14
QMAKE_CXXFLAGS = -std=c++1y

QT += core
QT -= gui
QT += network

TARGET = Simulator
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    Simulator.cpp \
    RobotState.cpp \
    communication/Communication.cpp \
    communication/CommunicationTcpSocket.cpp \
    communication/CommunicationTcpSocketServer.cpp

HEADERS += \
    Simulator.h \
    RobotState.h \
    communication/Communication.h \
    communication/CommunicationTcpSocket.h \
    communication/CommunicationTcpSocketServer.h

