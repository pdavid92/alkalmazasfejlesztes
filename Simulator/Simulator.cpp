#include "Simulator.h"

Simulator::Simulator(unsigned int port)
    : communication(port), state()
{
    // Fogadni akarjuk a parancsokat
    connect(&communication, SIGNAL(dataReady(QDataStream&)), this, SLOT(dataReady(QDataStream&)));
    // Periodikusan futnia kell a szimulációnak
    connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));
}

void Simulator::start(float intervalSec)
{
    dt = intervalSec;
    state.setStatus(RobotState::Status::Stopped);
    state.setTimestamp(0);
    state.setX(0.0F);
    state.setV(0.0F);
    state.setA(0.0F);
    state.setLinePos(8);

    // Vonalszenzor inicializálása
    QVector<float> tempVector(16);
    for (int i = 0; i < tempVector.size(); i++)
    {
        tempVector[i] = 0.01 * (rand() % 80);
    }
    tempVector[8] = 4.7 + 0.01 * (rand() % 30);
    tempVector[7] = 2.0 + 0.01 * (rand() % 100);
    tempVector[9] = 2.0 + 0.01 * (rand() % 100);
    state.setLinesensor(tempVector);

    timer.start((long)(intervalSec*1000.0F));
}

void Simulator::tick()
{
    // A robot állapotainak dekódolása
    switch(state.status())
    {
        case RobotState::Status::Stopped:
            state.setA(0.0F);
            state.setV(0.0F);
            break;

        case RobotState::Status::ConstantVelocityForward:
            state.setA(0.0F);
            break;

        case RobotState::Status::ConstantVelocityBackward:
            state.setA(0.0F);
            break;

        case RobotState::Status::AccelerateForward:
            state.setA(1.0F);
            break;

        case RobotState::Status::AccelerateBackward:
            state.setA(-1.0F);
            break;

        case RobotState::Status::StoppingForward:
            state.setA(-1.0F);
            break;

        case RobotState::Status::StoppingBackward:
            state.setA(1.0F);
            break;

        case RobotState::Status::Reset:
            Q_UNREACHABLE();
            break;

        case RobotState::Status::GoWithConstantVelocity:
            Q_UNREACHABLE();
            break;

        case RobotState::Status::Break:
            Q_UNREACHABLE();
            break;

        default:
            Q_UNREACHABLE();
    }

    // Fizikai szimuláció, sebesség, hely számítása
    state.setTimestamp(state.timestamp() + dt);
    state.setX(state.x() + state.v()*dt);
    state.setV(state.v() + state.a()*dt);

    // Ha nem áll a kocsi, akkorv véletlenszerűen kiválasztjuk hogy hol legyen a vonal közepe
    if (state.status() != RobotState::Status::Stopped)
    {
        // Ha a kocsi kisodródik az út szélére, a szabályzó visszahúzza középre
        if (state.linePos()==0 || state.linePos()==15)
        {
            state.setLinePos(6 + (rand() % 4));
        }
        // Amúgy a vonal helyzete max. +-2-t változik
        else
        {
            int tempPos = (state.linePos()-2) + (rand() % 5);

            if (tempPos > 15)
            {
                tempPos = 15;
            }
            else if (tempPos <0)
            {
                tempPos = 0;
            }
            else
            {
                state.setLinePos(tempPos);
            }
        }

        QVector<float> tempVector(16);

        // Feltöltjük a vonalszenzor vektorát 0.0-0.5 közötti random számokkal
        for (int i = 0; i < tempVector.size(); i++)
        {
            tempVector[i] = 0.01 * (rand() % 80);
        }

        // Azt az elemet ahol a vonal van egy 4.5-5.0 közötti számmal töltjük fel
        tempVector[state.linePos()] = 4.7 + 0.01 * (rand() % 30);

        // A két mellette lévőt pedig egy 2.0-2.5 közötti értékre
        if (state.linePos() > 0)
        {
            tempVector[state.linePos()-1] = 2.0 + 0.01 * (rand() % 100);
        }
        if (state.linePos() < 15)
        {
            tempVector[state.linePos()+1] = 2.0 + 0.01 * (rand() % 100);
        }

        state.setLinesensor(tempVector);
    }

    // Lassulás során ha elérjük a nullát megállunk és "Stopped"
    // állapotba térünk
    if ((state.status() == RobotState::Status::StoppingForward && state.v() <= 0) ||
        (state.status() == RobotState::Status::StoppingBackward && state.v() >= 0))
    {
        state.setStatus(RobotState::Status::Stopped);
    }

    // A maximális sebesség előre menetnél 10, tolatásnál 5, ha elérjük
    // nem gyorsítunk tovább, és állandó sebesség állapotba térünk
    if (state.v() > 10.0)
    {
        state.setStatus(RobotState::Status::ConstantVelocityForward);
        state.setV(10.0F);
    }
    if (state.v() < -5.0)
    {
        state.setStatus(RobotState::Status::ConstantVelocityBackward);
        state.setV(-5.0F);
    }

    qDebug() << "Simulator: tick (" << state.timestamp()
             << "): állapot=" << state.getStatusName()
             << ", x=" << state.x()
             << ", v=" << state.v()
             << ", a=" << state.a()
             << ", vonal pozíciója=" << state.linePos();

    // Állapot küldése
    if (communication.isConnected())
    {
       communication.send(state);
    }
}

void Simulator::dataReady(QDataStream &inputStream)
{
    RobotState receivedState;
    receivedState.ReadFrom(inputStream);

    switch(receivedState.status())
    {
        case RobotState::Status::Reset:
            qDebug() << "Simulator: Reset parancs.";
            this->start(1.0F);
            break;

        case RobotState::Status::GoWithConstantVelocity:
            qDebug() << "Simulator: Állandó sebesség parancs.";
            if (state.v() > 0)
            {
               state.setStatus(RobotState::Status::ConstantVelocityForward);
            }
            else if (state.v() < 0)
            {
               state.setStatus(RobotState::Status::ConstantVelocityBackward);
            }
            else
            {
               state.setStatus(RobotState::Status::Stopped);
            }
            break;

        case RobotState::Status::Break:
            qDebug() << "Simulator: Fékezés parancs.";
            if (state.v() > 0)
            {
               state.setStatus(RobotState::Status::StoppingForward);
            }
            else if (state.v() < 0)
            {
               state.setStatus(RobotState::Status::StoppingBackward);
            }
            else
            {
               state.setStatus(RobotState::Status::Stopped);
            }
            break;

        case RobotState::Status::AccelerateForward:
            if (state.v() >= 0)
            {
               qDebug() << "Simulator: Gyorsítás előre parancs.";
               state.setStatus(RobotState::Status::AccelerateForward);
            }
            else
            {
               qDebug() << "HIBA: Érvénytelen parancs.";
            }
            break;

        case RobotState::Status::AccelerateBackward:
            if (state.v() <= 0)
            {
                qDebug() << "Simulator: Gyorsítás hátrafelé parancs.";
                state.setStatus(RobotState::Status::AccelerateBackward);
            }
            else
            {
                qDebug() << "HIBA: Érvénytelen parancs.";
            }
            break;

        case RobotState::Status::Stopped:
            qDebug() << "HIBA: Érvénytelen parancs.";
            break;

        case RobotState::Status::ConstantVelocityForward:
            qDebug() << "HIBA: Érvénytelen parancs.";
            break;

        case RobotState::Status::ConstantVelocityBackward:
            qDebug() << "HIBA: Érvénytelen parancs.";
            break;

        case RobotState::Status::StoppingForward:
            qDebug() << "HIBA: Érvénytelen parancs.";
            break;

        case RobotState::Status::StoppingBackward:
            qDebug() << "HIBA: Érvénytelen parancs.";
            break;

        default:
            Q_UNREACHABLE();
    }
}
