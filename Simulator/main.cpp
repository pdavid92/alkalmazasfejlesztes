#include <QCoreApplication>
#include "Simulator.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Simulator simulator(3333);
    simulator.start(1.0F);

    return a.exec();
}

