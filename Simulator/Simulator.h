#pragma once
#ifndef SIMULATOR_H
#define SIMULATOR_H
#include <QObject>
#include <QTimer>
#include "communication/CommunicationTcpSocketServer.h"
#include "RobotState.h"
#include <cstdlib>

/**
 * @brief A robot szimulátor osztály.
 *
 * Van egy belső, RobotState típusú állapota, amelyet egy QTimer segítségével
 * a start() függvény paramétereként megadott periodussal frissít,
 * majd minden szimulációs lépés után elküldi azt a kliens felé.
 * Létrehoz egy CommunicationTcpSocketServer objektumot a kommunikációhoz,
 * amihez a kliens a konstruktor paramétereként megadott porton
 * keresztül tud csatlakozni. A klienstől RobotState típusú objektumokat kap,
 * amiken keresztül a szimulátornak parancsokat lehet adni.
 */
class Simulator : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief A szimulátor konstruktora.
     * @param port  A port, amin a létrehozott szerver hallgatózik.
     */
    explicit Simulator(unsigned int port);
    ~Simulator() = default;

    /**
     * @brief Elindítja, vagy újraindítja szimulátort a default
     * paraméter értékekkel.
     * @param intervalSec   A szimulátor periódusideje.
     */
    void start(float intervalSec);

private:

    /**
     * @brief Belső szerver a kommunikációhoz.
     */
    CommunicationTcpSocketServer communication;

    /**
     * @brief Időzítő a tick() metódus periodikus hívására.
     */
    QTimer timer;

    /**
     * @brief A periódus idő (másodpercben), amilyen időnként
     * szeretnénk hogy a tick() függvény meghívódjon, és a
     * szimulátor szimulációt végezzen.
     */
    float dt;

    /**
     * @brief A szimulátor aktuális állapota.
     */
    RobotState state;

private slots:

    /**
     * @brief A timer hívja meg amikor túlcsordul,
     * ebben a függvényben számítjuk ki a robot állapotát a következő
     * időegységre, és küldjük azt tovább a kliens számára.
     */
    void tick();

    /**
     * @brief Új üzenet fogadásakor hívódik meg,
     * a beérkezett adatot kezeljük le benne.
     *
     * @param inputStream   A TCP socketen beérkező adatfolyam.
     */
    void dataReady(QDataStream& inputStream);
};

#endif // SIMULATOR_H
