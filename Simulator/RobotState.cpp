#include <QDebug>
#include "RobotState.h"

std::map<int,QString> RobotState::statusNames;

RobotState::RobotState()
{
    initStatusNames();
    QVector<float> tempVector(16,0);
    _linesensor = tempVector;
}

RobotState::RobotState(Status status, qint64 timestamp,
    float x, float v, float a, QVector<float> linesensor, int linePos)
    : _status(status), _timestamp(timestamp), _x(x), _v(v), _a(a),
      _linesensor(linesensor), _linePos(linePos)
{
    initStatusNames();
}

void RobotState::initStatusNames()
{
    if (statusNames.empty())
    {
        statusNames[(int)Status::Reset] = QString("Reset");
        statusNames[(int)Status::GoWithConstantVelocity] = QString("Állandó sebesség parancs");
        statusNames[(int)Status::Break] = QString("Fékezés parancs");
        statusNames[(int)Status::AccelerateForward] = QString("Gyorsítás előre");
        statusNames[(int)Status::AccelerateBackward] = QString("Gyorsítás hátra");
        statusNames[(int)Status::Stopped] = QString("Álló helyzet");
        statusNames[(int)Status::ConstantVelocityForward] = QString("Állandó sebesség (előre)");
        statusNames[(int)Status::ConstantVelocityBackward] = QString("Állandó sebesség (tolatás)");
        statusNames[(int)Status::StoppingForward] = QString("Előrehaladás lassulva");
        statusNames[(int)Status::StoppingBackward] = QString("Tolatás lassulva");
    }
}

QString RobotState::getStatusName() const
{
    auto it = statusNames.find((int)_status);
    Q_ASSERT(it != statusNames.end());
    return it->second;
}

void RobotState::WriteTo(QDataStream& stream) const
{
    stream << (qint32)_status;
    stream << _timestamp;
    stream << _x;
    stream << _v;
    stream << _a;
    stream << _linePos;
    stream << _linesensor;
}

void RobotState::ReadFrom(QDataStream& stream)
{
    qint32 tmpQint32;
    stream >> tmpQint32;
    _status = (Status)tmpQint32;
    stream >> _timestamp;
    stream >> _x;
    stream >> _v;
    stream >> _a;
    stream >> _linePos;
    stream >> _linesensor;
}

void RobotState::CopyFrom(const RobotState &other)
{
    _status = other._status;
    _timestamp = other._timestamp;
    _x = other._x;
    _v = other._v;
    _a = other._a;
    _linePos = other._linePos;
    _linesensor = other._linesensor;
}

QDataStream &operator<<(QDataStream& stream, const RobotState& state)
{
    state.WriteTo(stream);
    return stream;
}

QDataStream &operator>>(QDataStream& stream, RobotState& state)
{
    state.ReadFrom(stream);
    return stream;
}
