#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "StvApplication.h"

int main(int argc, char *argv[])
{
    StvApplication app(argc, argv);
    return app.exec();
}

