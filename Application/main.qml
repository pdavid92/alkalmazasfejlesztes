import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2

ApplicationWindow {
    visible: true
    title: qsTr("Hello World")
    width: 867
    height: 637

    signal simulate_cmdCpp()
    signal reset_cmdCpp()
    signal gofw_cmdCpp()
    signal gobw_cmdCpp()
    signal goconstv_cmdCpp()
    signal stop_cmdCpp()  

    ScrollView{
        width:parent.width
        height: parent.height
        horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOn
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn

        Flickable{
            contentWidth: 850
            contentHeight: 620

            MainForm {

                id: mainFormControl
                //Az eseménykezelőkben tovább hívjuk az itteni signalokat a C++ oldal felé.
                onSimulate_cmd:{
                    simulate_cmdCpp();
                }
                onReset_cmd: {
                   reset_cmdCpp();
                }
                onGofw_cmd: {
                    gofw_cmdCpp();
                }
                onGobw_cmd: {
                    gobw_cmdCpp();
                }
                onGoconstv_cmd: {
                    goconstv_cmdCpp();
                }
                onStop_cmd: {
                    stop_cmdCpp();
                }
            }
        }
    }
}

