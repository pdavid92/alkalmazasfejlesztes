#pragma once
#ifndef MAINWINDOWSEVENTHANDLING_H
#define MAINWINDOWSEVENTHANDLING_H
#include <QObject>
#include <QDebug>
#include <QQuickView>
#include <QTimer>

class RobotProxy;
class QQmlContext;
class RobotStateHistory;
class QQuickItem;
class QQmlApplicationEngine;

/**
 * @brief Ez az osztály tartalmazza a felhasználó felület (GUI) eseménykezelőit.
 *
 * A függőségeit a konstruktoron kereszül kapja meg és kapcsolódik a GUI
 * signaljaihoz.
 *
 * A ConnectQmlSignals() metódust meg kell hívni a QML gyökér objektummal, hogy
 * a megfelelő signalokat be tudja kötni.
 */
class MainWindowsEventHandling : public QObject
{
    Q_OBJECT

public:
    /** Konstruktor
     * @param robot A robot proxy példány
     * @param qmlContext    A QML context objektum
     * @param history   A használt RobotStateHistory objektum.
     */
    MainWindowsEventHandling(RobotProxy& robot, QQmlContext &qmlContext, RobotStateHistory &history);

    /** Destruktor
     */
    ~MainWindowsEventHandling() = default;

    /** Csatlakoztatja a QML oldali signalokat. Az után kell meghívni, hogy a QML környezet felállt.
     * @param rootObject    A QML gyökérelem.
     */
    void ConnectQmlSignals(QObject *rootObject);

    /**
     * @brief historyInit
     * History változásának szimulálása, mert ettől kezdve léteznek a QML oldalon
     * a C++ oldalról származó változók. (Ezt elhagyva referencia hibákat kapnánk a QML oldalon
     * egészen addig, amíg az első üzenet meg nem jönne a szimulátortól.
     */
    void historyInit();

public slots:
    /** A "Kapcsolódás" nyomógomb (MainForm.qml/simulateBTN) eseménykezelője.
     *  A robottal való kapcsolat feléptésére szolgál.
    */
    void simulate_cmd();

    /** Az "Újraindítás" nyomógomb (MainForm.qml/resetBTN) eseménykezelője.
     *  Reset parancsot küld a robot felé.
    */
    void reset_cmd();

    /** A "Gyorsítás előre" nyomógomb (MainForm.qml/gofwBTN) eseménykezelője.
     *  Gyorsítás előre parancsot küld a robotnak.
    */
    void gofw_cmd();

    /** A "Gyorsítás hátra" nyomógomb (MainForm.qml/gobwBTN) eseménykezelője.
     *  Gyorsítás hátra parancsot küld a robotnak.
    */
    void gobw_cmd();

    /** Az "Egyenletes sebesség" nyomgomb (MainForm.qml/goconstvBTN) eseménykezelője.
     *  Egyenletes sebbességgel való haladás parancsot küld a robtonak.
    */
    void goconstv_cmd();

    /** A "Fékezés" nyomógomb (MainForm.qml/stopBTN) eseménykezelője. Fékezés parancsot
     *  küld a robotnak.
    */
    void stop_cmd();

    /** Azt jelzi, hogy változott az állapot history. (Tipikusan mert új állapot érkezett a robottól.)
     * Frissíti a QML számára elérhetővé tett, C++ oldali változókat (propertyket) és
     * kiváltja a historyContextUpdated() signalt.
    */
    void historyChanged();

private slots:

        /** Azt jelzi, hogy a beállítot időzítő ciklusdeje lejárt,
         *  ekkor hívja meg a robot kapcsolódási állapotát jelző függvényt
         *  és teszi bele annak bool visszatérési értékét egy qml-nek átadott változóba
        */
    void tick();

signals:
    /** Jelzi, hogy változott a megjelenítés számára az adatmodell.
     * Ilyenkor az érintett QML elemek (a grafikon) újrarajzolják magukat.
     */
    void historyContextUpdated();

private:
    /** Időzítő a robot kapcsolódási állapotának periodikus lekérdezéséhez*/
    QTimer timer;

    /** A használt robot proxy. */
    RobotProxy& robot;

    /** \addtogroup Hivatkozások adatkötéshez
     *  @{
     */
    /** QML context a robot adatok frissítéséhez. */
    QQmlContext &qmlContext;
    /**
     * @brief rootObject
     */
    QObject* rootObject;
    /** A history objektum. */
    RobotStateHistory &history;
    /** @}*/

    /** Segédfüggvény QML elemek rekurzív megkeresésére.
     * @param rootObject A QML gyökérelem, amivel a keresést kezdjük.
     * @param name Az objectName (QML elemek tulajdonsága), amit keresünk.
     */
    static QQuickItem* FindItemByName(QObject *rootObject, const QString& name);

    /** Segédfüggvény QML elemek rekurzív megkeresésére.
     * A FindItemByName(QObject *rootObject, const QString& name) használja.
     * @param nodes Azon node-ok listája, melyekben (rekurzívan) az adott nevű elemet keressük.
     * @param name  Az objectName (QML elemek tulajdonsága), amit keresünk.
     */
    static QQuickItem* FindItemByName(QList<QObject*> nodes, const QString& name);
};

#endif // MAINWINDOWSEVENTHANDLING_H
