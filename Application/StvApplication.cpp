#include "StvApplication.h"

StvApplication::StvApplication(int argc, char *argv[])
    : QApplication(argc, argv), engine(), history(), communication(),
      robot(history, communication), handler(robot, *engine.rootContext(), history)
{

    // Szimulálunk egy history változást, mert attól kezdve léteznek a QML oldalon
    //  a C++ oldalról származó változók. (Különben referencia hibákat kapnánk a QML oldalon
    //  egészen addig, amíg az első üzenet meg nem jönne a szimulátortól.
    handler.historyInit();

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    // A QML gyökérelemre szükségünk van ahhoz, hogy tudjunk hivatkozni a QML-es elemekre.
    auto rootObjects = engine.rootObjects();
    if (rootObjects.size() == 0)
    {
        qDebug() << "HIBA: Nem sikerült létrehozni a QML környezetet.";
        return;
    }
    // A QML környezet is felállt, bekötjük a signalokat a QML és C++ oldal között.
    QObject *rootObject = rootObjects[0];
    // A handler beköti a saját signaljait.
    handler.ConnectQmlSignals(rootObject);

    // Bekötjük a nyomógombok signaljait.
    QObject::connect(rootObject, SIGNAL(simulate_cmdCpp()),
                     &handler, SLOT(simulate_cmd()));
    QObject::connect(rootObject, SIGNAL(reset_cmdCpp()),
                     &handler, SLOT(reset_cmd()));
    QObject::connect(rootObject, SIGNAL(gofw_cmdCpp()),
                     &handler, SLOT(gofw_cmd()));
    QObject::connect(rootObject, SIGNAL(gobw_cmdCpp()),
                     &handler, SLOT(gobw_cmd()));
    QObject::connect(rootObject, SIGNAL(goconstv_cmdCpp()),
                     &handler, SLOT(goconstv_cmd()));
    QObject::connect(rootObject, SIGNAL(stop_cmdCpp()),
                     &handler, SLOT(stop_cmd()));
}

