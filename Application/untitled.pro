TEMPLATE = app

QT += qml quick
CONFIG += c++14
QMAKE_CXXFLAGS = -std=c++1y

QT += qml quick widgets
QT += network

SOURCES += main.cpp \
    MainWindowsEventHandling.cpp \
    RobotProxy.cpp \
    RobotStateHistory.cpp \
    StvApplication.cpp \
    RobotState.cpp \
    communication/Communication.cpp \
    communication/CommunicationTcpSocket.cpp \
    communication/CommunicationTcpSocketClient.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    MainWindowsEventHandling.h \
    RobotProxy.h \
    RobotStateHistory.h \
    StvApplication.h \
    RobotState.h \
    communication/Communication.h \
    communication/CommunicationTcpSocket.h \
    communication/CommunicationTcpSocketClient.h

