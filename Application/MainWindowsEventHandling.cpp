#include "MainWindowsEventHandling.h"
#include "RobotProxy.h"
#include <QQmlContext>
#include "RobotStateHistory.h"

#include <QQuickItem>
#include <QQmlApplicationEngine>

MainWindowsEventHandling::MainWindowsEventHandling(
        RobotProxy &robot, QQmlContext& qmlContext, RobotStateHistory& history)
    : robot(robot), qmlContext(qmlContext), history(history)
{
    QObject::connect(&history, SIGNAL(historyChanged()), this, SLOT(historyChanged()));
    QObject::connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));
}

void MainWindowsEventHandling::simulate_cmd()
{
    robot.simulate();
}

void MainWindowsEventHandling::reset_cmd()
{
    robot.reset();
}

void MainWindowsEventHandling::gofw_cmd()
{
    robot.gofw();
}

void MainWindowsEventHandling::gobw_cmd()
{
    robot.gobw();
}

void MainWindowsEventHandling::goconstv_cmd()
{
    robot.goconstv();
}

void MainWindowsEventHandling::stop_cmd()
{
    robot.stop();
}

void MainWindowsEventHandling::tick()
{
    //Kapcsolódás vizsgálatát reprezentáló változó készítése qml oldalra
    qmlContext.setContextProperty(QStringLiteral("isconnected"), QVariant::fromValue(robot.isConnected()));
}

void MainWindowsEventHandling::historyInit()
{
    // Ahhoz, hogy frissüljenek a QML oldali adatok, frissíteni kell a változók összekapcsolását.
    qmlContext.setContextProperty(QStringLiteral("historyModel"), QVariant::fromValue(history.stateList));
    qmlContext.setContextProperty(QStringLiteral("currentState"), QVariant::fromValue(history.currentState));
    qmlContext.setContextProperty(QStringLiteral("historyGraphTimestamps"), QVariant::fromValue(history.graphTimestamps));
    qmlContext.setContextProperty(QStringLiteral("historyGraphVelocity"), QVariant::fromValue(history.graphVelocities));
    qmlContext.setContextProperty(QStringLiteral("historyGraphAcceleration"), QVariant::fromValue(history.graphAcceleration));
    qmlContext.setContextProperty(QStringLiteral("isconnected"), QVariant::fromValue(robot.isConnected()));
    //kapcsolódás vizsgálat időzítő indítás
    timer.start(500);

    // Jelzünk a QML controloknak, hogy újrarajzolhatják magukat, beállítottuk az új értékeket.
    emit historyContextUpdated();
}

void MainWindowsEventHandling::historyChanged()
{
    // Ahhoz, hogy frissüljenek a QML oldali adatok, frissíteni kell a változók összekapcsolását.
    qmlContext.setContextProperty(QStringLiteral("historyModel"), QVariant::fromValue(history.stateList));
    qmlContext.setContextProperty(QStringLiteral("currentState"), QVariant::fromValue(history.currentState));
    qmlContext.setContextProperty(QStringLiteral("historyGraphTimestamps"), QVariant::fromValue(history.graphTimestamps));
    qmlContext.setContextProperty(QStringLiteral("historyGraphVelocity"), QVariant::fromValue(history.graphVelocities));
    qmlContext.setContextProperty(QStringLiteral("historyGraphAcceleration"), QVariant::fromValue(history.graphAcceleration));
    //vonalszenzor értékeinek másolása
    QVector<float> sensorValues = history.currentState->linesensor();

    for (int i=0; i<sensorValues.size(); i++)
    {
       //Vonalkövetést jelző téglalapok nevének beállítása
       QObject *rect = FindItemByName(rootObject, "rect"+QString::number(i));

       if(rect)
       {
           // Vonalkövetést jelző téglalapok magasságának állítása
          rect->setProperty("height", QString::number(qRound(36*sensorValues[i])));
          if(i==history.currentState->linePos()) //más színnel jelezve, ha ehhez tartozik a legmagasabb fesz.érték
              rect->setProperty("color", "#D81B60");
          else
              rect->setProperty("color", "#880E4F");
       }
    }

    // Jelzünk a QML controloknak, hogy újrarajzolhatják magukat, beállítottuk az új értékeket.
    emit historyContextUpdated();
}

QQuickItem* MainWindowsEventHandling::FindItemByName(QList<QObject*> nodes, const QString& name)
{
    for(int i = 0; i < nodes.size(); i++)
    {
        // Node keresése
        if (nodes.at(i) && nodes.at(i)->objectName() == name)
        {
            return dynamic_cast<QQuickItem*>(nodes.at(i));
        }
        // Gyerekekben keresés
        else if (nodes.at(i) && nodes.at(i)->children().size() > 0)
        {
            QQuickItem* item = FindItemByName(nodes.at(i)->children(), name);
            if (item)
                return item;
        }
    }
    // Nem találtuk.
    return nullptr;
}

QQuickItem* MainWindowsEventHandling::FindItemByName(QObject *rootObject, const QString& name)
{
    return FindItemByName(rootObject->children(), name);
}

void MainWindowsEventHandling::ConnectQmlSignals(QObject *rootObject)
{
    this->rootObject = rootObject;

    QQuickItem *historyGraph = FindItemByName(rootObject,QString("historyGraph"));
    if (historyGraph)
    {
        QObject::connect(this, SIGNAL(historyContextUpdated()), historyGraph, SLOT(requestPaint()));
    }
    else
    {
        qDebug() << "HIBA: Nem találom a historyGraph objektumot a QML környezetben.";
    }
}
