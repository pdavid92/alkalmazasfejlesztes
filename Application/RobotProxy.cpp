#include "RobotProxy.h"
#include "RobotStateHistory.h"
#include "communication/CommunicationTcpSocketClient.h"
#include "RobotState.h"

RobotProxy::RobotProxy(RobotStateHistory& history, CommunicationTcpSocketClient &communication)
    : history(history), communication(communication)
{
    // Jelzést kérünk, ha új robot állapot (RobotState) érkezik.
    QObject::connect(&communication, SIGNAL(dataReady(QDataStream&)), this, SLOT(dataReady(QDataStream&)));
}

void RobotProxy::dataReady(QDataStream &stream)
{
    // Új robot állapto érkezett, elmentjük a historyba.
    //  (Onnan vesszük majd azt is, hogy mi az aktuális állapot.)
    RobotState state;
    state.ReadFrom(stream);
    history.Add(state);
}

void RobotProxy::gofw()
{
    // Gyorsítás előre parancs küldése.
    RobotState newState;
    newState.setStatus(RobotState::Status::AccelerateForward);
    communication.send(newState);
    qDebug() << "Gyorsítás előre parancs elküldve";
}

void RobotProxy::gobw()
{
    // Gyorsítás hátra parancs küldése.
    RobotState newState;
    newState.setStatus(RobotState::Status::AccelerateBackward);
    communication.send(newState);
    qDebug() << "Gyorsítás hátra parancs elküldve";
}

void RobotProxy::goconstv()
{
    // Egyenletes sebességgel haladás parancs küldése.
    RobotState newState;
    newState.setStatus(RobotState::Status::GoWithConstantVelocity);
    communication.send(newState);
    qDebug() << "Egyenletes sebesség parancs elküldve";
}

void RobotProxy::stop()
{
    // Megállás parancs küldése
    RobotState newState;
    newState.setStatus(RobotState::Status::Break);
    communication.send(newState);
    qDebug() << "Stop parancs elküldve.";
}

void RobotProxy::simulate()
{
    //Kapcsolódás
    communication.connect(QString("localhost"),3333);
    qDebug() << "Kapcsolódás.";
}

void RobotProxy::reset()
{
    //Reset parancs küldése
    RobotState newState;
    newState.setStatus(RobotState::Status::Reset);
    communication.send(newState);
    qDebug()<< "Reset parancs elküldése";
}

