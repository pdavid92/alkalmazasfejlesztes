#pragma once
#ifndef ROBOTPROXY_H
#define ROBOTPROXY_H
#include <QObject>
#include "RobotState.h"
#include "communication/CommunicationTcpSocketClient.h"

class RobotStateHistory;
class SocketClient;

/**
 * @brief A robot proxyja.
 *
 * Ezen kereszül lehet adatok küldeni és állapotjelzéseket fogadni a robottól.
 * A korábbi állapotokat a kapott RobotStateHistory objektumban tárolja.
 *
 * A konstruktor köti a dataReady() slotot a kommunikációs objektumhoz és kezeli az adatfogadást.
 */
class RobotProxy : public QObject
{
    Q_OBJECT

public:
    /**
     * @brief Konstruktor.
     * @param history   A használandó RobotStateHistory példány.
     * @param communication A használandó kommunikációs objektum.
     */
    RobotProxy(RobotStateHistory& history, CommunicationTcpSocketClient& communication);
    /** Destruktor*/
    ~RobotProxy() = default;

    /**
     * @brief Kapcsolódás kezdeményezése a robot felé.
     */
    void simulate();

    /**
     * @brief Robot újraindítása parancs küldése.
     */
    void reset();

    /**
     * @brief Gyorsítás előre parancs küldése a robotnak.
     */
    void gofw();

    /**
     * @brief Gyorsítás hátra parancs küldése a robotnak.
     */
    void gobw();

    /**
     * @brief Egyenletes sebességgel haladás parancs küldése a robotnak.
     */
    void goconstv();

    /**
     * @brief Megállási parancs küldése a robotnak.
     */
    void stop();

    /**
      * @brief Segédfüggvény a kapcsolódás állapotának lekrédezésére
      * True érétkkel tér vissza, ha a robot kapcsolódva van a kliens alkalmazáshoz,
      * és False-t ad, ha nincs kapcsolat.
      */
    bool isConnected(){ return communication.isConnected(); }


public slots:
    /**
     * Akkor hívódik, ha új állapot érkezett a robottól.
     * Feldolgozza és eltárolja az üzenetet.
     *
     * A konstruktor köti a kommunikációs objektum üzenet beérkezést jelző
     * signaljához.
     *
     * @param stream    A bejövő adatfolyam, amiből az állapotot ki kell olvasni.
     */
    void dataReady(QDataStream& stream);

private:
    /** A korábbi és aktuális állapotot tároló RobotStateHistory példány. */
    RobotStateHistory& history;

    /** A kommunikációs objektum */
    CommunicationTcpSocketClient& communication;
};

#endif // ROBOTPROXY_H
