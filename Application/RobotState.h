#pragma once
#ifndef ROBOTSTATE_H
#define ROBOTSTATE_H
#include <QDataStream>
#include <QString>
#include <QVector>

/**
 * @brief A robot állapotát írja le egy adott időpillanatban.
 *
 * A kommunikáció során ilyen objektumokat küldünk át a robot és a kliens között.
 * Ha a robot küldi a kliens felé, akkor az aktuális állapotot írja le,
 * a kliens pedig parancsokat tud küldeni a segítségével a robot felé.
 */
class RobotState : public QObject
{
    Q_OBJECT

public:

    /**
     * @brief A robot aktuális állapotát leíró enum osztály
     */
    enum class Status
    {
        // Parancsok

        /**
         * @brief Reset parancs. A szimulátor alaphelyzetbe kerül.
         */
        Reset = 0,

        /**
         * @brief Haladás állandó sebességgel parancs.
         */
        GoWithConstantVelocity = 1,

        /**
         * @brief Fékezés parancs.
         */
        Break = 2,

        /**
         * @brief Gyorsítás előre 1 m/s^2-el. Parancs és állapot is egyben.
         */
        AccelerateForward = 3,

        /**
         * @brief Gyorsítás hátra 1 m/s^2-el. Parancs és állapot is egyben.
         */
        AccelerateBackward = 4,


        // Állapotok

        /**
         * @brief Álló állapot. A robot egy helyben áll, sebessége és gyorsulása is 0.
         */
        Stopped = 5,

        /**
         * @brief Mozgás előre állandó sebességgel állapot, a gyorsulás 0.
         */
        ConstantVelocityForward = 6,

        /**
         * @brief Tolatás állandó sebességgel állapot, a gyorsulás 0.
         */
        ConstantVelocityBackward = 7,

        /**
         * @brief Fékezés előre menetel közben -1 m/s^2-el. Állapot.
         */
        StoppingForward = 8,

        /**
         * @brief Fékezés tolatás közben -1 m/s^2-el. Állapot.
         */
        StoppingBackward = 9
    };

    /**
     * @brief Konstruktor.
     *
     * Inicializálja az állapotok szöveges neveit, és
     * a linesensor() vektort egy 16 elemű, 0-kat tartalmazó vektorként.
     */
    RobotState();

    /**
     * @brief Konstruktor megadott paraméterértékekkel.
     *
     * Inicializálja az állapotok szöveges neveit, és a
     * paramétereket a megadott értékekkel.
     *
     * @param status        Robot állapot
     * @param timestamp     Időbélyeg
     * @param x             Pozíció
     * @param v             Sebesség
     * @param a             Gyorsulás
     * @param linesensor    A vonalszenzoron mért feszültségértékek
     * @param linePos       A vonal pozíciója
     */
    RobotState(Status status, qint64 timestamp,
        float x, float v, float a, QVector<float> linesensor, int linePos);

    /**
     * @brief Destruktor.
     */
    ~RobotState() = default;

    // A NOTIFY signalokat nem implementáljuk, mert nincs rájuk szükség.

    /**
     * @brief Állapot (vagy parancs)
     */
    Q_PROPERTY(Status status READ status WRITE setStatus MEMBER _status NOTIFY statusChanged)
    Status status() const { return _status; }
    void setStatus(const Status status) { _status = status; }

    /**
     * @brief Időbélyeg (ms)
     */
    Q_PROPERTY(float timestamp READ timestamp WRITE setTimestamp MEMBER _timestamp NOTIFY timestampChanged)
    qint64 timestamp() const { return _timestamp; }
    void setTimestamp(const qint64 timestamp) { _timestamp = timestamp; }

    /**
     * @brief Pozíció (méter)
     */
    Q_PROPERTY(float x READ x WRITE setX MEMBER _x NOTIFY xChanged)
    float x() const { return _x; }
    void setX(float x) { _x = x; }

    /**
     * @brief Sebesség (m/s)
     */
    Q_PROPERTY(float v READ v WRITE setV MEMBER _v NOTIFY vChanged)
    float v() const { return _v; }
    void setV(float v) { _v = v; }

    /**
     * @brief Gyorsulás (m/s2)
     */
    Q_PROPERTY(float a READ a WRITE setA MEMBER _a NOTIFY aChanged)
    float a() const { return _a; }
    void setA(float a) { _a = a; }

    /**
     * @brief A vonal pozíciója 0-15-ös skálán
     */
    Q_PROPERTY(int linePos READ linePos WRITE setLinePos MEMBER _linePos NOTIFY linePosChanged)
    int linePos() const { return _linePos; }
    void setLinePos(int linePos) { _linePos = linePos; }

    /**
     * @brief A vonalszenzoron mért feszültségértékek vektora (V)
     */
    Q_PROPERTY(QVector<float> linesensor READ linesensor WRITE setLinesensor MEMBER _linesensor NOTIFY linesensorChanged)
    QVector<float> linesensor() const { return _linesensor; }
    void setLinesensor(QVector<float>& linesensor) { _linesensor = linesensor; }

    /**
     * @brief Az aktuális állapot QStringként.
     */
    // In QML, it will be accessible as model.statusName
    Q_PROPERTY(QString statusName READ getStatusName NOTIFY statusChanged)

    /**
     * @brief Sorosítja az objektumot a megadott streambe.
     *
     * @param stream    A kimenő adatfolyam
     */
    void WriteTo(QDataStream& stream) const;

    /**
     * @brief Beolvassa az objektumot a streamből.
     *
     * @param stream    A bejövő adatfolyam
     */
    void ReadFrom(QDataStream& stream);

    /**
     * @brief Másolat készítése másik RobotState objektumról.
     *
     * @param other     A lemásolni kívánt objektum
     */
    // Erre azért van szükség, mert a QObject-ek másolására speciális szabályok vonatkoznak.
    void CopyFrom(const RobotState& other);

    /**
     * @brief Olvaható formában visszaadja az állapot nevét.
     *
     * @returns Az állapotnév olvasható formában
     */
    QString getStatusName() const;

signals:

    // Ezeket a signalokat most nem használjuk
    void statusChanged();
    void timestampChanged();
    void xChanged();
    void vChanged();
    void aChanged();
    void linePosChanged();
    void linesensorChanged();

private:

    /**
     * @brief A robot állapota / Parancsok a robot számára
     */
    Status _status;

    /**
     * @brief Időbélyeg (s)
     */
    qint64 _timestamp;

    /**
     * @brief Pozíció (m)
     */
    float _x;

    /**
     * @brief Sebesség (m/s)
     */
    float _v;

    /**
     * @brief Gyorsulás (m/s2)
     */
    float _a;

    /**
     * @brief A vonalszenzoron mért feszültségértékek vektora (V)
     */
    QVector<float> _linesensor;

    /**
     * @brief A vonal pozíciója 0-15-ös skálán
     */
    int _linePos;

    /**
     * @brief Az állapotok és szöveges verziójuk közti megfeleltetés. A getStatusName() használja.
     */
    static std::map<int,QString> statusNames;

    /**
     * @brief Beállítja a statusNames értékeit. A konstruktor hívja.
     */
    void initStatusNames();
};

/**
 * @brief Beburkolja a RobotState.WriteTo metódust.
 */
QDataStream &operator<<(QDataStream &, const RobotState &);

/**
 * @brief Beburkolja a RobotState.ReadFrom metódust.
 */
QDataStream &operator>>(QDataStream &, RobotState &);

#endif // ROBOTSTATE_H
