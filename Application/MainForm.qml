import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1




Item {

    //signal-ok a parancs-gombokhoz
    signal simulate_cmd;
    signal reset_cmd;
    signal gofw_cmd;
    signal gobw_cmd;
    signal stop_cmd;
    signal goconstv_cmd;


            //Parancsküldő gombok és aktuális állapotkijelzés
            GroupBox
            {
                id: commandsGB
                title: "Parancsok"
                anchors.left : parent.left
                anchors.top : parent.top
                width: 200
                height: 220
                anchors.margins: 5

                ColumnLayout
                {
                    anchors.fill: parent
                    anchors.margins: 2
                    spacing: 0
                    Button
                    {
                       id: simulateBTN
                       text: qsTr("Kapcsolódás")
                       anchors.top: parent.top
                       anchors.left: parent.left
                       anchors.right: parent.right
                       onClicked: simulate_cmd()
                    }
                    Button
                    {
                       id: resetBTN
                       text: qsTr("Újraindítás")
                       anchors.top: simulateBTN.bottom
                       anchors.topMargin: 2
                       anchors.left: parent.left
                       anchors.right: parent.right
                       onClicked: reset_cmd()
                    }
                    Text
                    {
                        id:constTXT
                        text: "Aktuális állapot:"
                        anchors.top: resetBTN.bottom
                        anchors.topMargin: 8
                        anchors.left: parent.left
                    }
                    Text
                    {
                        id:stateTXT
                        text: (currentState!=null ? currentState.statusName : "?")
                        anchors.top: constTXT.bottom
                        anchors.topMargin: 2
                        anchors.left: parent.left
                        anchors.leftMargin: 5
                    }
                    Button
                    {
                       id: gofwBTN
                       text: qsTr("Gyorsítás előre")
                       anchors.top: stateTXT.bottom
                       anchors.topMargin: 8
                       anchors.left: parent.left
                       anchors.right: parent.right
                       onClicked: gofw_cmd()
                    }
                    Button
                    {
                       id: gobwBTN
                       text: qsTr("Gyorsítás hátra")
                       anchors.top: gofwBTN.bottom
                       anchors.topMargin: 2
                       anchors.left: parent.left
                       anchors.right: parent.right
                       onClicked: gobw_cmd()
                    }
                    Button
                    {
                       id: goconstvBTN
                       text: qsTr("Egyenletes sebesség")
                       anchors.top: gobwBTN.bottom
                       anchors.topMargin: 2
                       anchors.left: parent.left
                       anchors.right: parent.right
                       onClicked: goconstv_cmd()
                    }
                    Button
                    {
                       id: stopBTN
                       text: qsTr("Fékezés")
                       anchors.top: goconstvBTN.bottom
                       anchors.topMargin: 2
                       anchors.left: parent.left
                       anchors.right: parent.right
                       onClicked: stop_cmd()
                    }
                }
            }

            //Vonalkövetés aktuális állapota
            GroupBox
            {
                id: lineGB
                title: "Vonalkövetés állapota"
                anchors.left: commandsGB.right
                anchors.leftMargin: 5
                anchors.top : parent.top
                anchors.topMargin: 5
                width: 400
                height: 220

                Rectangle
                {
                    height: 1
                    width: 384
                    anchors.bottom: lineRL.bottom
                    anchors.bottomMargin: 182
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    color: "gray"
                }
                Rectangle
                {
                    height: 1
                    width: 384
                    anchors.bottom: lineRL.bottom
                    anchors.bottomMargin: 146
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    color: "gray"
                }
                Rectangle
                {
                    height: 1
                    width: 384
                    anchors.bottom: lineRL.bottom
                    anchors.bottomMargin: 110
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    color: "gray"
                }
                Rectangle
                {
                    height: 1
                    width: 384
                    anchors.bottom: lineRL.bottom
                    anchors.bottomMargin: 74
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    color: "gray"
                }
                Rectangle
                {
                    height: 1
                    width: 384
                    anchors.bottom: lineRL.bottom
                    anchors.bottomMargin: 38
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    color: "gray"
                }

                RowLayout
                {
                    id: lineRL
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 3
                    anchors.left: parent.left
                    anchors.leftMargin: 3

                    //vonalkövetés megjelenítéséhez, ezek magasságához kell a tömb értékeket kötni
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect0"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect1"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect2"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect3"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect4"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect5"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect6"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect7"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect8"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect9"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect10"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect11"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect12"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect13"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect14"
                    }
                    Rectangle{
                        anchors.bottom: parent.bottom
                        anchors.margins: 2
                        width:19
                        height: 0
                        color: "#880E4F"
                        objectName: "rect15"
                    }
                }
            }

            //Folyamatos adatkiíratáshoz
            Component {
                // ID, hogy tudjuk a listánál hivatkozni erre, mint a lista delegatejére.
                id: stateDelegate
                Row {
                    // Itt a model az, ami a list egyik eleme. (Bármi is legyen majd az.)
                    Text { text: "Állapot : "+(model.statusName)
                            width: 190
                         }
                    Text { text: " X : " + model.x.toFixed(3)
                           width: 70
                         }
                    Text {
                        text: " V : " + model.v.toFixed(3)
                        color: "red"
                        width: 70
                        }
                    Text {
                        text: " A : " + model.a.toFixed(3)
                        color: "blue"
                        width: 70
                        }
                }
            }

            //Aktuális adatok (állapot, idő, x, v, a, vonal helye)
            GroupBox
            {
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.left: lineGB.right
                anchors.leftMargin: 5
                height: 220
                width: 230
                id: aktdataGB
                title: "Gyűjtött adatok"
                // Oszlopba rendezett további elemek
                ColumnLayout {
                    anchors.left: parent.left
                    anchors.leftMargin: 3
                    anchors.top: parent.top
                    anchors.topMargin: 15
                    // Sima szövegek (Text elemek), amiknek az értéke egy a C++ oldalon definiált currentState
                    //  értékétől függ. (Ha az értéke null, akkor "?" jelenik meg.)
                    // A currentState-et a MainWindowsEventHandling::historyChanged metódus regisztrálja be, hogy
                    //  látható legyen a QML oldalról is. (Hivatkozás a RobotStateHistory::currentState-re.)
                    Text { text: " Állapot: " + (currentState!=null ? currentState.statusName : "?") }
                    Text { text: " Eltelt idő: " + (currentState!=null ? currentState.timestamp+" s" : "?") }
                    Text { text: " Pozíció: " + (currentState!=null ? currentState.x.toFixed(3)+ " m" : "?") }
                    Text { text: " Sebesség: " + (currentState!=null ? currentState.v.toFixed(3)+ " m/s" : "?") }
                    Text { text: " Gyorsulás: " + (currentState!=null ? currentState.a.toFixed(3)+ " m/s^2" : "?") }
                    Text { text: " Vonal helye: " + (currentState!=null ? (currentState.linePos+1) + ". szenzor" : "-")
                           color: "#FF4081"
                         }
                }

            }

            //Sebesség és gyorsulás idődiagramja
            GroupBox
            {
                id:vaGB
                title:"Sebesség, Gyorsulás"
                width: 400
                height: 360
                anchors.left: parent.left
                anchors.leftMargin: 5
                anchors.top: commandsGB.bottom
                anchors.topMargin: 5

                HistoryGraph {
                    id: historyGraph
                    // Az objectName akkor jó, ha C++ oldalról kell megkeresnünk egy QML oldalon definiált
                    //  objektumot a findChild metódus rekurzív hívásaival.
                    objectName: "historyGraph"
                    anchors.fill: parent

                    // A RowLayout erre az elemre vonatkozó elhelyezés beállításai.
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    Layout.minimumWidth: 200
                    Layout.preferredWidth: 400
                    Layout.minimumHeight: 150

                    // Ezek pedig a HistoryGraph tulajdonságai, amiket majd ott definiálunk,
                    //  itt pedig értéket adunk nekik. Az alábbi változókat (pl. historyGraphTimestamps)
                    //  szintén a MainWindowsEventHandling::historyChanged metódus teszi elérhetővé
                    //  a QML oldal számára.
                    // Ezek az értékek C++ oldalon folyamatosan változnak. Minden változás esetén
                    //  lefut a MainWindowsEventHandling::historyChanged és ezeket újraregisztrálja a QML
                    //  oldal számára, így frissülni fog a HistoryGraph tulajdonság is.
                    graphTimestamps: historyGraphTimestamps
                    graphVelocities: historyGraphVelocity
                    graphAccelerations: historyGraphAcceleration
                }


            }

            //Grafikon egyes pontjainak adatai (állapot, x, v, a) folyamatosan felsorolva
            GroupBox
            {
                id: graphdataGB
                title: "Grafikon adatai"
                width: 430
                height: 360
                anchors.left: vaGB.right
                anchors.leftMargin: 10
                anchors.top: commandsGB.bottom
                anchors.topMargin: 5
                ScrollView {
                    // A scrollohzató elem igazítása a szölő RowLayouthoz.
                    // Itt a ScrollViewon belül adjuk meg, hogy a RowLayoutban
                    //  mik legyenek a rá (ScrollViewra) vonatkozó méret beállítások,
                    //  mert ezeket a RowLayout kezeli ebben az esetben.

                    anchors.fill: parent

                    // Itt jön a tényleges lista.
                    ListView {
                        id: stateHistoryList
                        // A model az, ahonnan az adatokat vesszük.
                        // A historyModel változót a MainWindowsEventHandling::historyChanged metódus teszi
                        //  elérhetővé a QML oldalon is.
                        //  Típusa QList<QObject*>, a tárolt pointerek valójában RobotState-ekre mutatnak.
                        model: historyModel
                        // A delegate megadása, vagyis hogy egy listaelem hogyan jelenjen meg.
                        //  (Már fentebb definiáltuk.)
                        delegate: stateDelegate

                        // Eseménykezelő, az elemek darabszámának változása esetén a kijelölést
                        //  a legalsó elemre viszi. Ezzel oldja meg, hogy folyamatosan scrollozódjon
                        //  a lista és a legutoló elem mindig látható legyen.
                        onCountChanged: {
                            stateHistoryList.currentIndex = stateHistoryList.count - 1;
                        }
                    }
                }
            }

            //Kapcsolódás állapotát jelző téglalap
            Rectangle
            {
                id:connectRTG
                anchors.top:graphdataGB.bottom
                anchors.topMargin: 8
                anchors.left: parent.left
                anchors.leftMargin: 5
                width: 10
                height: 10
                color: isconnected==true ? "green" : "red"
            }

            //Kapcsolódás állapotát jelző üzenet
            Text
            {
                id:connectinfoTXT
                anchors.top:graphdataGB.bottom
                anchors.topMargin: 5
                anchors.left: connectRTG.right
                anchors.leftMargin: 5
                width:820
                height: 10
                text: isconnected==true ? "Kapcsolódva a szimulátorhoz" : "Nincs kapcsolat a szimulátorral"
                }

}







